window.__imported__ = window.__imported__ || {};
window.__imported__["ce-32411-mr-view@1x/layers.json.js"] = [
	{
		"objectId": "7392B801-536D-467E-95B7-46BD06E4363F",
		"kind": "artboard",
		"name": "topbar",
		"originalName": "topbar",
		"maskFrame": null,
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1280,
			"height": 102
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "B727B012-11A3-434C-98E9-CADCFD632637",
				"kind": "group",
				"name": "top",
				"originalName": "top",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1280,
					"height": 102
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-top-qjcyn0iw.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1280,
						"height": 102
					}
				},
				"children": []
			}
		]
	},
	{
		"objectId": "7486581B-0F73-4DD4-A151-E5AFB4313363",
		"kind": "artboard",
		"name": "sidebar",
		"originalName": "sidebar",
		"maskFrame": null,
		"layerFrame": {
			"x": 2360,
			"y": 102,
			"width": 290,
			"height": 698
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "6CD8CF5D-C3DE-4294-99CA-7C9BE0626316",
				"kind": "group",
				"name": "side",
				"originalName": "side",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 290,
					"height": 1872
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-side-nkneoeng.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 290,
						"height": 1872
					}
				},
				"children": [
					{
						"objectId": "27A830B2-D19E-432B-80DF-FB4A434930F8",
						"kind": "group",
						"name": "todo",
						"originalName": "todo",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 10,
							"width": 259,
							"height": 46
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-todo-mjdbodmw.png",
							"frame": {
								"x": 18,
								"y": 10,
								"width": 259,
								"height": 46
							}
						},
						"children": [
							{
								"objectId": "879EB7BE-020B-48B0-8478-A67C4A64114B",
								"kind": "group",
								"name": "grey_button_default",
								"originalName": "grey-button-default",
								"maskFrame": null,
								"layerFrame": {
									"x": 158,
									"y": 10,
									"width": 83,
									"height": 35
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-grey_button_default-odc5rui3.png",
									"frame": {
										"x": 158,
										"y": 10,
										"width": 83,
										"height": 35
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "E010A6F0-2990-498A-83C1-19EED5318C40",
						"kind": "group",
						"name": "assignee",
						"originalName": "assignee",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 74,
							"width": 259,
							"height": 55
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-assignee-rtaxmee2.png",
							"frame": {
								"x": 18,
								"y": 74,
								"width": 259,
								"height": 55
							}
						},
						"children": []
					},
					{
						"objectId": "F3080FDF-0B5E-462D-B0A4-960545981FA2",
						"kind": "group",
						"name": "milestone",
						"originalName": "milestone",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 147,
							"width": 259,
							"height": 55
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-milestone-rjmwodbg.png",
							"frame": {
								"x": 18,
								"y": 147,
								"width": 259,
								"height": 55
							}
						},
						"children": []
					},
					{
						"objectId": "38D1F0EE-7052-4F6C-9523-74F921707280",
						"kind": "group",
						"name": "labels",
						"originalName": "labels",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 220,
							"width": 259,
							"height": 65
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-labels-mzhemuyw.png",
							"frame": {
								"x": 18,
								"y": 220,
								"width": 259,
								"height": 65
							}
						},
						"children": [
							{
								"objectId": "54E43E52-7D4C-4C2B-A828-F4F6736E79AA",
								"kind": "group",
								"name": "label_frontend",
								"originalName": "label--frontend",
								"maskFrame": null,
								"layerFrame": {
									"x": 18,
									"y": 243,
									"width": 70,
									"height": 25
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-label_frontend-ntrfndnf.png",
									"frame": {
										"x": 18,
										"y": 243,
										"width": 70,
										"height": 25
									}
								},
								"children": []
							},
							{
								"objectId": "9DBF05A2-D228-4EC5-828E-2736E6F5D9DC",
								"kind": "group",
								"name": "label_pick_into_stable",
								"originalName": "label--pick-into-stable",
								"maskFrame": null,
								"layerFrame": {
									"x": 96,
									"y": 243,
									"width": 104,
									"height": 25
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-label_pick_into_stable-ourcrja1.png",
									"frame": {
										"x": 96,
										"y": 243,
										"width": 104,
										"height": 25
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "C35D2F42-40D4-4FAE-B3CA-F36CE77B458F",
						"kind": "group",
						"name": "participants",
						"originalName": "participants",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 303,
							"width": 259,
							"height": 64
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-participants-qzm1rdjg.png",
							"frame": {
								"x": 18,
								"y": 303,
								"width": 259,
								"height": 64
							}
						},
						"children": [
							{
								"objectId": "98712D2F-AC9A-4BD6-89EF-5A93967382AC",
								"kind": "group",
								"name": "user_picture",
								"originalName": "user-picture",
								"maskFrame": {
									"x": 0,
									"y": 0,
									"width": 22,
									"height": 22
								},
								"layerFrame": {
									"x": 18,
									"y": 326,
									"width": 24,
									"height": 24
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-user_picture-otg3mtje.png",
									"frame": {
										"x": 18,
										"y": 326,
										"width": 24,
										"height": 24
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "AEC3F2ED-DD92-4762-AA73-774AE6CAD263",
						"kind": "group",
						"name": "notifications",
						"originalName": "notifications",
						"maskFrame": null,
						"layerFrame": {
							"x": 10,
							"y": 377,
							"width": 270,
							"height": 94
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-notifications-quvdm0yy.png",
							"frame": {
								"x": 10,
								"y": 377,
								"width": 270,
								"height": 94
							}
						},
						"children": [
							{
								"objectId": "8A32100D-15EF-4175-8DC8-961A0F31ED08",
								"kind": "group",
								"name": "button",
								"originalName": "button",
								"maskFrame": null,
								"layerFrame": {
									"x": 194,
									"y": 377,
									"width": 83,
									"height": 35
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-button-oeezmjew.png",
									"frame": {
										"x": 194,
										"y": 377,
										"width": 83,
										"height": 35
									}
								},
								"children": []
							}
						]
					}
				]
			}
		]
	},
	{
		"objectId": "1BE8C3BE-7CAE-4395-8F18-6C05A6815AFB",
		"kind": "artboard",
		"name": "MR",
		"originalName": "MR",
		"maskFrame": null,
		"layerFrame": {
			"x": 2760,
			"y": 102,
			"width": 990,
			"height": 1273
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "2BA42F02-808C-416D-9D33-7257083CABCE",
				"kind": "group",
				"name": "mrviewcontent",
				"originalName": "mrviewcontent",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 4,
					"width": 991,
					"height": 2128
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "32E93EE9-B61F-43D8-ADDE-3024750BC02F",
						"kind": "group",
						"name": "titlebar",
						"originalName": "titlebar",
						"maskFrame": null,
						"layerFrame": {
							"x": 15,
							"y": 4,
							"width": 962,
							"height": 52
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-titlebar-mzjfotnf.png",
							"frame": {
								"x": 15,
								"y": 4,
								"width": 962,
								"height": 52
							}
						},
						"children": [
							{
								"objectId": "A985842B-D96A-4531-B91A-59AFB9705A6E",
								"kind": "group",
								"name": "meta",
								"originalName": "meta",
								"maskFrame": null,
								"layerFrame": {
									"x": 15,
									"y": 16,
									"width": 694,
									"height": 25
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-meta-qtk4ntg0.png",
									"frame": {
										"x": 15,
										"y": 16,
										"width": 694,
										"height": 25
									}
								},
								"children": [
									{
										"objectId": "792B6E11-1AD5-4E07-BE46-BD1B685A5AC2",
										"kind": "group",
										"name": "Group_19_Copy",
										"originalName": "Group 19 Copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 15,
											"y": 15.0645161290322,
											"width": 232,
											"height": 25
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_19_Copy-nzkyqjzf.png",
											"frame": {
												"x": 15,
												"y": 15.0645161290322,
												"width": 232,
												"height": 25
											}
										},
										"children": [
											{
												"objectId": "49A6FCFB-3354-45FB-B806-8B17E4D2E790",
												"kind": "group",
												"name": "Group_10",
												"originalName": "Group 10",
												"maskFrame": null,
												"layerFrame": {
													"x": 23,
													"y": 16,
													"width": 224,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_10-ndlbnkzd.png",
													"frame": {
														"x": 23,
														"y": 16,
														"width": 224,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "617708F7-C00D-4280-ADCC-D553AA3A6064",
														"kind": "group",
														"name": "Group_2",
														"originalName": "Group 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 23,
															"y": 22,
															"width": 51,
															"height": 14
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_2-nje3nza4.png",
															"frame": {
																"x": 23,
																"y": 22,
																"width": 51,
																"height": 14
															}
														},
														"children": [
															{
																"objectId": "52D9BF3E-CF73-4B9D-9B46-A88A45AD614A",
																"kind": "group",
																"name": "Group_20_Copy",
																"originalName": "Group 20 Copy",
																"maskFrame": null,
																"layerFrame": {
																	"x": 130,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_20_Copy-ntjeoujg.png",
																	"frame": {
																		"x": 130,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															},
															{
																"objectId": "0D23D613-2878-4E93-9FCC-9B9B2C92CE76",
																"kind": "group",
																"name": "Group_7_Copy_2",
																"originalName": "Group 7 Copy 2",
																"maskFrame": null,
																"layerFrame": {
																	"x": 128,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_7_Copy_2-meqym0q2.png",
																	"frame": {
																		"x": 128,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															}
														]
													}
												]
											},
											{
												"objectId": "D3041174-B643-4ACE-A7E9-DCAFA8DCC13E",
												"kind": "group",
												"name": "Group",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 192,
													"y": 20,
													"width": 35,
													"height": 16
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": []
											}
										]
									},
									{
										"objectId": "61D50717-9C8D-44AB-88E5-5CA98D2D6BAF",
										"kind": "group",
										"name": "Group_19",
										"originalName": "Group 19",
										"maskFrame": null,
										"layerFrame": {
											"x": 15,
											"y": 15.0645161290322,
											"width": 233,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_19-njfenta3.png",
											"frame": {
												"x": 15,
												"y": 15.0645161290322,
												"width": 233,
												"height": 25
											}
										},
										"children": [
											{
												"objectId": "7E9CD92B-834A-4E55-BD22-E34FCBFD4FA8",
												"kind": "text",
												"name": "ID",
												"originalName": "ID",
												"maskFrame": null,
												"layerFrame": {
													"x": 208,
													"y": 23,
													"width": 33,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "!5142",
													"css": [
														"/* !5142: */",
														"font-family: HelveticaNeue;",
														"font-size: 14px;",
														"color: rgba(0,0,0,0.55);",
														"letter-spacing: 0;"
													]
												},
												"image": {
													"path": "images/Layer-ID-n0u5q0q5.png",
													"frame": {
														"x": 208,
														"y": 23,
														"width": 33,
														"height": 11
													}
												},
												"children": []
											},
											{
												"objectId": "92C0291C-91B2-4E42-BD04-1914876E62C7",
												"kind": "text",
												"name": "Type",
												"originalName": "Type",
												"maskFrame": null,
												"layerFrame": {
													"x": 92,
													"y": 22,
													"width": 100,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Merge Request",
													"css": [
														"/* Merge Request: */",
														"font-family: SourceSansPro-Semibold;",
														"font-size: 16px;",
														"color: #5C5C5C;"
													]
												},
												"image": {
													"path": "images/Layer-Type-otjdmdi5.png",
													"frame": {
														"x": 92,
														"y": 22,
														"width": 100,
														"height": 15
													}
												},
												"children": []
											},
											{
												"objectId": "CC6886BC-D919-4067-9382-3A1EB23E19C0",
												"kind": "group",
												"name": "Group_101",
												"originalName": "Group 10",
												"maskFrame": null,
												"layerFrame": {
													"x": 23,
													"y": 16,
													"width": 225,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_10-q0m2odg2.png",
													"frame": {
														"x": 23,
														"y": 16,
														"width": 225,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "DA2BB058-1869-412E-9014-78D9B01371AD",
														"kind": "group",
														"name": "Group_21",
														"originalName": "Group 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 23,
															"y": 22,
															"width": 51,
															"height": 14
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_2-reeyqkiw.png",
															"frame": {
																"x": 23,
																"y": 22,
																"width": 51,
																"height": 14
															}
														},
														"children": [
															{
																"objectId": "430A5098-49D7-45BD-B3CE-E4AE83E77303",
																"kind": "group",
																"name": "Group_20_Copy_2",
																"originalName": "Group 20 Copy 2",
																"maskFrame": null,
																"layerFrame": {
																	"x": 130,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_20_Copy_2-ndmwqtuw.png",
																	"frame": {
																		"x": 130,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															},
															{
																"objectId": "7E20CAC9-CEBD-4745-A87A-7E7DA826BBE5",
																"kind": "group",
																"name": "Group_7_Copy",
																"originalName": "Group 7 Copy",
																"maskFrame": null,
																"layerFrame": {
																	"x": 128,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_7_Copy-n0uymenb.png",
																	"frame": {
																		"x": 128,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															}
														]
													}
												]
											},
											{
												"objectId": "CF173697-DC44-482A-9E1F-54156CFF7CD3",
												"kind": "group",
												"name": "Group1",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 192,
													"y": 20,
													"width": 35,
													"height": 16
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": []
											}
										]
									},
									{
										"objectId": "39ED8753-0BB6-434A-8C63-19FD8D366A38",
										"kind": "group",
										"name": "label_merge",
										"originalName": "label-merge",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 15.0645161290322,
											"width": 68,
											"height": 25
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-label_merge-mzlfrdg3.png",
											"frame": {
												"x": 16,
												"y": 15.0645161290322,
												"width": 68,
												"height": 25
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "78A2AC2D-6DEC-4D07-B61A-D460A82386AB",
								"kind": "group",
								"name": "button_edit",
								"originalName": "button-edit",
								"maskFrame": null,
								"layerFrame": {
									"x": 927,
									"y": 10,
									"width": 47,
									"height": 34
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-button_edit-nzhbmkfd.png",
									"frame": {
										"x": 927,
										"y": 10,
										"width": 47,
										"height": 34
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "1B6C94B5-8078-4BAD-BBF0-BBC2CB820455",
						"kind": "group",
						"name": "branches",
						"originalName": "branches",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 579,
							"width": 979,
							"height": 366
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-branches-mui2qzk0.png",
							"frame": {
								"x": 0,
								"y": 579,
								"width": 979,
								"height": 366
							}
						},
						"children": [
							{
								"objectId": "C0371A84-73C7-4D8A-8039-E607809286E6",
								"kind": "group",
								"name": "widget",
								"originalName": "widget",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 629,
									"width": 958,
									"height": 316
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "B392E72A-D228-40F8-98FD-6512A325E885",
										"kind": "group",
										"name": "body",
										"originalName": "body",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 688,
											"width": 958,
											"height": 242
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-body-qjm5mku3.png",
											"frame": {
												"x": 16,
												"y": 688,
												"width": 958,
												"height": 242
											}
										},
										"children": [
											{
												"objectId": "1B4E04C6-4D06-4258-A927-579C7CC07E40",
												"kind": "group",
												"name": "stop_environment_button_copy_2",
												"originalName": "stop-environment-button copy 2",
												"maskFrame": null,
												"layerFrame": {
													"x": 574,
													"y": 802,
													"width": 99,
													"height": 25
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-stop_environment_button_copy_2-mui0rta0.png",
													"frame": {
														"x": 574,
														"y": 802,
														"width": 99,
														"height": 25
													}
												},
												"children": []
											},
											{
												"objectId": "C91F5FF2-B32E-4E8A-998B-39EAF6AA0FF6",
												"kind": "group",
												"name": "Group_23",
												"originalName": "Group 23",
												"maskFrame": null,
												"layerFrame": {
													"x": 509,
													"y": 802,
													"width": 55,
													"height": 25
												},
												"visible": true,
												"metadata": {
													"opacity": 0.35
												},
												"image": {
													"path": "images/Layer-Group_23-qzkxrjvg.png",
													"frame": {
														"x": 509,
														"y": 802,
														"width": 55,
														"height": 25
													}
												},
												"children": []
											},
											{
												"objectId": "50844134-C8E1-469E-85D7-D4C6F26CEFB3",
												"kind": "text",
												"name": "command_line_instructions",
												"originalName": "command-line-instructions",
												"maskFrame": null,
												"layerFrame": {
													"x": 68,
													"y": 915,
													"width": 466,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "You can also merge or inspect manually using the command line instructions",
													"css": [
														"/* You can also merge o: */",
														"font-family: SourceSansPro-It;",
														"font-size: 15px;",
														"color: #333333;",
														"letter-spacing: 0;",
														"line-height: 20px;"
													]
												},
												"image": {
													"path": "images/Layer-command_line_instructions-nta4ndqx.png",
													"frame": {
														"x": 68,
														"y": 915,
														"width": 466,
														"height": 15
													}
												},
												"children": []
											},
											{
												"objectId": "78763FDC-5861-49AF-8BD4-B00802F988B3",
												"kind": "group",
												"name": "Group_9_Copy_5",
												"originalName": "Group 9 Copy 5",
												"maskFrame": null,
												"layerFrame": {
													"x": 31,
													"y": 690,
													"width": 22,
													"height": 22
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9_Copy_5-nzg3njng.png",
													"frame": {
														"x": 31,
														"y": 690,
														"width": 22,
														"height": 22
													}
												},
												"children": []
											},
											{
												"objectId": "FF9B7594-95B4-40FB-8BE0-99A0B6A8484F",
												"kind": "group",
												"name": "Group_9",
												"originalName": "Group 9",
												"maskFrame": null,
												"layerFrame": {
													"x": 31,
													"y": 803,
													"width": 22,
													"height": 22
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9-rky5qjc1.png",
													"frame": {
														"x": 31,
														"y": 803,
														"width": 22,
														"height": 22
													}
												},
												"children": []
											},
											{
												"objectId": "2E6AFB43-8A4F-4EF9-9ADD-6BC4FCA8F31A",
												"kind": "group",
												"name": "Group_18",
												"originalName": "Group 18",
												"maskFrame": null,
												"layerFrame": {
													"x": 16,
													"y": 688,
													"width": 958,
													"height": 213
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_18-mku2quzc.png",
													"frame": {
														"x": 16,
														"y": 688,
														"width": 958,
														"height": 213
													}
												},
												"children": [
													{
														"objectId": "668BAE3F-29A8-47EF-9635-ABC848CE6B15",
														"kind": "group",
														"name": "Group_211",
														"originalName": "Group 21",
														"maskFrame": null,
														"layerFrame": {
															"x": 31,
															"y": 688,
															"width": 387,
															"height": 27
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_21-njy4qkff.png",
															"frame": {
																"x": 31,
																"y": 688,
																"width": 387,
																"height": 27
															}
														},
														"children": [
															{
																"objectId": "4D688105-51EB-4234-B488-C7B7B8026338",
																"kind": "group",
																"name": "Group_9_Copy_3",
																"originalName": "Group 9 Copy 3",
																"maskFrame": null,
																"layerFrame": {
																	"x": 31,
																	"y": 690,
																	"width": 22,
																	"height": 22
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_9_Copy_3-neq2odgx.png",
																	"frame": {
																		"x": 31,
																		"y": 690,
																		"width": 22,
																		"height": 22
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "5305274B-5EDC-4C5D-A0E1-4E705B5F920E",
														"kind": "group",
														"name": "Group_20",
														"originalName": "Group 20",
														"maskFrame": null,
														"layerFrame": {
															"x": 17,
															"y": 720,
															"width": 956,
															"height": 38
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_20-ntmwnti3.png",
															"frame": {
																"x": 17,
																"y": 720,
																"width": 956,
																"height": 38
															}
														},
														"children": [
															{
																"objectId": "5701E442-E1EF-4471-B220-DDC63A459A0F",
																"kind": "group",
																"name": "Group_22",
																"originalName": "Group 22",
																"maskFrame": null,
																"layerFrame": {
																	"x": 58,
																	"y": 729,
																	"width": 62,
																	"height": 22
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_22-ntcwmuu0.png",
																	"frame": {
																		"x": 58,
																		"y": 729,
																		"width": 62,
																		"height": 22
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "ECD4CD8E-BCAD-40FE-9FAD-95C196AA1FA5",
														"kind": "group",
														"name": "MR_body_text",
														"originalName": "MR-body-text",
														"maskFrame": null,
														"layerFrame": {
															"x": 67,
															"y": 752,
															"width": 206,
															"height": 15
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-MR_body_text-runenene.png",
															"frame": {
																"x": 67,
																"y": 752,
																"width": 206,
																"height": 15
															}
														},
														"children": [
															{
																"objectId": "5DB86953-F6FE-4A21-9854-FDB3392A777D",
																"kind": "group",
																"name": "stop_environment_button_copy",
																"originalName": "stop-environment-button copy",
																"maskFrame": null,
																"layerFrame": {
																	"x": 448,
																	"y": 741,
																	"width": 130,
																	"height": 24
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-stop_environment_button_copy-nurcody5.png",
																	"frame": {
																		"x": 448,
																		"y": 741,
																		"width": 130,
																		"height": 24
																	}
																},
																"children": []
															},
															{
																"objectId": "9FD424A8-BDAE-45D8-A298-21F30C12BAD9",
																"kind": "group",
																"name": "environment_buttons",
																"originalName": "environment buttons",
																"maskFrame": null,
																"layerFrame": {
																	"x": 62,
																	"y": 773,
																	"width": 475,
																	"height": 127
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "3EBAD3C1-B299-4A73-B1CE-971016CC8958",
																		"kind": "group",
																		"name": "Group_14",
																		"originalName": "Group 14",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 481,
																			"y": 773,
																			"width": 56,
																			"height": 26
																		},
																		"visible": false,
																		"metadata": {
																			"opacity": 1
																		},
																		"children": [
																			{
																				"objectId": "D1028258-0604-41E6-AF50-548966019717",
																				"kind": "group",
																				"name": "stop_environment_button_copy_3",
																				"originalName": "stop-environment-button copy 3",
																				"maskFrame": null,
																				"layerFrame": {
																					"x": 511,
																					"y": 774,
																					"width": 26,
																					"height": 25
																				},
																				"visible": true,
																				"metadata": {
																					"opacity": 1
																				},
																				"image": {
																					"path": "images/Layer-stop_environment_button_copy_3-rdewmjgy.png",
																					"frame": {
																						"x": 511,
																						"y": 774,
																						"width": 26,
																						"height": 25
																					}
																				},
																				"children": []
																			},
																			{
																				"objectId": "5B7AA63B-4FD1-428B-BFE6-4A5A3B3FAEBD",
																				"kind": "group",
																				"name": "stop_environment_button_copy_4",
																				"originalName": "stop-environment-button copy 4",
																				"maskFrame": null,
																				"layerFrame": {
																					"x": 481,
																					"y": 774,
																					"width": 31,
																					"height": 25
																				},
																				"visible": true,
																				"metadata": {
																					"opacity": 1
																				},
																				"image": {
																					"path": "images/Layer-stop_environment_button_copy_4-nui3que2.png",
																					"frame": {
																						"x": 481,
																						"y": 774,
																						"width": 31,
																						"height": 25
																					}
																				},
																				"children": []
																			}
																		]
																	},
																	{
																		"objectId": "1B37F750-36A9-4CE7-BDD7-82C6038BA317",
																		"kind": "group",
																		"name": "Group_13_Copy",
																		"originalName": "Group 13 Copy",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 62,
																			"y": 875,
																			"width": 399,
																			"height": 25
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"children": [
																			{
																				"objectId": "9530A4BD-3D2E-4391-9C2C-698BE1C1CC20",
																				"kind": "group",
																				"name": "stop_environment_button_copy_5",
																				"originalName": "stop-environment-button copy 5",
																				"maskFrame": null,
																				"layerFrame": {
																					"x": 62,
																					"y": 875,
																					"width": 159,
																					"height": 25
																				},
																				"visible": false,
																				"metadata": {
																					"opacity": 1
																				},
																				"image": {
																					"path": "images/Layer-stop_environment_button_copy_5-otuzmee0.png",
																					"frame": {
																						"x": 62,
																						"y": 875,
																						"width": 159,
																						"height": 25
																					}
																				},
																				"children": []
																			},
																			{
																				"objectId": "4A897B51-8923-4552-8663-E34A7E2C5AF5",
																				"kind": "group",
																				"name": "stop_environment_button_copy_21",
																				"originalName": "stop-environment-button copy 2",
																				"maskFrame": null,
																				"layerFrame": {
																					"x": 302,
																					"y": 875,
																					"width": 159,
																					"height": 25
																				},
																				"visible": false,
																				"metadata": {
																					"opacity": 1
																				},
																				"image": {
																					"path": "images/Layer-stop_environment_button_copy_2-nee4otdc.png",
																					"frame": {
																						"x": 302,
																						"y": 875,
																						"width": 159,
																						"height": 25
																					}
																				},
																				"children": []
																			}
																		]
																	}
																]
															},
															{
																"objectId": "C6398F03-DEE1-42DA-B21E-B253E51C991D",
																"kind": "group",
																"name": "Group_13",
																"originalName": "Group 13",
																"maskFrame": null,
																"layerFrame": {
																	"x": 721,
																	"y": 687,
																	"width": 243,
																	"height": 30
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "730E4EF7-C80A-4D77-BA74-1381E07A9FF8",
																		"kind": "group",
																		"name": "Group_6",
																		"originalName": "Group 6",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 721,
																			"y": 687,
																			"width": 243,
																			"height": 30
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group_6-nzmwrtrf.png",
																			"frame": {
																				"x": 721,
																				"y": 687,
																				"width": 243,
																				"height": 30
																			}
																		},
																		"children": []
																	}
																]
															},
															{
																"objectId": "F528EDD6-CD03-4142-A03B-11BFBD33EEA9",
																"kind": "group",
																"name": "Group_4",
																"originalName": "Group 4",
																"maskFrame": null,
																"layerFrame": {
																	"x": 477,
																	"y": 772,
																	"width": 30,
																	"height": 30
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_4-rjuyoeve.png",
																	"frame": {
																		"x": 477,
																		"y": 772,
																		"width": 30,
																		"height": 30
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "F6B2F730-CB09-4067-AC94-DCC106660C76",
														"kind": "group",
														"name": "Group_9_Copy_2",
														"originalName": "Group 9 Copy 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 37,
															"y": 811,
															"width": 10,
															"height": 11
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy_2-rjzcmky3.png",
															"frame": {
																"x": 37,
																"y": 811,
																"width": 10,
																"height": 11
															}
														},
														"children": []
													},
													{
														"objectId": "4CA69F47-D74B-446F-8569-EF59AF1D1BF9",
														"kind": "group",
														"name": "Group_9_Copy",
														"originalName": "Group 9 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 31,
															"y": 746,
															"width": 22,
															"height": 22
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy-nenbnjlg.png",
															"frame": {
																"x": 31,
																"y": 746,
																"width": 22,
																"height": 22
															}
														},
														"children": []
													}
												]
											}
										]
									},
									{
										"objectId": "803CB4EA-21A3-419B-8A68-CB5F9920DEBA",
										"kind": "group",
										"name": "header",
										"originalName": "header",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 646,
											"width": 958,
											"height": 29
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-header-odazq0i0.png",
											"frame": {
												"x": 16,
												"y": 646,
												"width": 958,
												"height": 29
											}
										},
										"children": []
									},
									{
										"objectId": "7A38BF51-D571-49CC-BAA5-16E41DA25358",
										"kind": "group",
										"name": "container",
										"originalName": "container",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 629,
											"width": 958,
											"height": 316
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-container-n0ezoejg.png",
											"frame": {
												"x": 16,
												"y": 629,
												"width": 958,
												"height": 316
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "BEAB147C-ABD3-4919-9BE7-B42E9AA7272E",
								"kind": "group",
								"name": "divider",
								"originalName": "divider",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 589,
									"width": 958,
									"height": 1
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-divider-qkvbqje0.png",
									"frame": {
										"x": 16,
										"y": 589,
										"width": 958,
										"height": 1
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "E47D7FB6-4EF2-47E7-AB57-4543A8241AA6",
						"kind": "group",
						"name": "tab_tabs",
						"originalName": "tab_tabs",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 1005,
							"width": 958,
							"height": 51
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-tab_tabs-rtq3rddg.png",
							"frame": {
								"x": 16,
								"y": 1005,
								"width": 958,
								"height": 51
							}
						},
						"children": [
							{
								"objectId": "92EB9DB1-08AB-4F67-8BBF-7EC6D97CFC9B",
								"kind": "group",
								"name": "unresolved_discussions",
								"originalName": "unresolved_discussions",
								"maskFrame": null,
								"layerFrame": {
									"x": 724,
									"y": 1014,
									"width": 250,
									"height": 30
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-unresolved_discussions-otjfqjle.jpg",
									"frame": {
										"x": 724,
										"y": 1014,
										"width": 250,
										"height": 30
									}
								},
								"children": [
									{
										"objectId": "462093BF-C37C-475E-8657-FBD82B68B74D",
										"kind": "group",
										"name": "Group_28",
										"originalName": "Group 28",
										"maskFrame": null,
										"layerFrame": {
											"x": 724,
											"y": 1014,
											"width": 250,
											"height": 30
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_28-ndyymdkz.png",
											"frame": {
												"x": 724,
												"y": 1014,
												"width": 250,
												"height": 30
											}
										},
										"children": [
											{
												"objectId": "4A3DBBBD-AEE2-488D-8433-DE3A027CCC52",
												"kind": "group",
												"name": "Group_9_Copy_8",
												"originalName": "Group 9 Copy 8",
												"maskFrame": null,
												"layerFrame": {
													"x": 734,
													"y": 1022,
													"width": 14,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9_Copy_8-neezrejc.png",
													"frame": {
														"x": 734,
														"y": 1022,
														"width": 14,
														"height": 15
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "75A5C7F8-D402-42F7-8CB4-4B7B3F0E42F3",
								"kind": "group",
								"name": "discussion",
								"originalName": "discussion",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 1005,
									"width": 111,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "5134384D-A571-4648-AB03-ED05460D8A19",
										"kind": "text",
										"name": "title",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 27,
											"y": 1023,
											"width": 65,
											"height": 12
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Discussion",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-ntezndm4.png",
											"frame": {
												"x": 27,
												"y": 1023,
												"width": 65,
												"height": 12
											}
										},
										"children": []
									},
									{
										"objectId": "03195B0A-E0B6-4E03-B484-A2DF045B3650",
										"kind": "group",
										"name": "underline",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 1053,
											"width": 111,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-mdmxotvc.png",
											"frame": {
												"x": 16,
												"y": 1053,
												"width": 111,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "C2E41C2D-B6E9-493F-A82E-DE6B91739249",
										"kind": "group",
										"name": "count_badge",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 96,
											"y": 1021,
											"width": 23,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-qzjfndfd.png",
											"frame": {
												"x": 96,
												"y": 1021,
												"width": 23,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "BE382E14-DA99-49FA-8730-0D0D057BFF42",
										"kind": "group",
										"name": "bg",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 1005,
											"width": 111,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-qkuzodjf.png",
											"frame": {
												"x": 16,
												"y": 1005,
												"width": 111,
												"height": 50
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "AB78B1DC-A74B-4366-B57A-DE1AD913ACFD",
								"kind": "group",
								"name": "commits",
								"originalName": "commits",
								"maskFrame": null,
								"layerFrame": {
									"x": 127,
									"y": 1005,
									"width": 101,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "ECD0C900-37BF-48A4-AC8A-A96D2F32DDDC",
										"kind": "text",
										"name": "title1",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 137,
											"y": 1023,
											"width": 56,
											"height": 12
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Commits",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-runemem5.png",
											"frame": {
												"x": 137,
												"y": 1023,
												"width": 56,
												"height": 12
											}
										},
										"children": []
									},
									{
										"objectId": "E58AD6DE-8E5C-431A-AB27-17B7352166C2",
										"kind": "group",
										"name": "underline1",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 127,
											"y": 1053,
											"width": 101,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-rtu4quq2.png",
											"frame": {
												"x": 127,
												"y": 1053,
												"width": 101,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "950FEB57-1DEB-4369-A9A0-1F8F092B3715",
										"kind": "group",
										"name": "count_badge1",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 197,
											"y": 1021,
											"width": 21,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-otuwrkvc.png",
											"frame": {
												"x": 197,
												"y": 1021,
												"width": 21,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "059567A8-1936-4482-8F12-5E962C3CD38C",
										"kind": "group",
										"name": "bg1",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 127,
											"y": 1005,
											"width": 101,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-mdu5nty3.png",
											"frame": {
												"x": 127,
												"y": 1005,
												"width": 101,
												"height": 50
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "A4F0D694-7C41-498D-B8A7-272795D50A7C",
								"kind": "group",
								"name": "pipelines",
								"originalName": "pipelines",
								"maskFrame": null,
								"layerFrame": {
									"x": 228,
									"y": 1005,
									"width": 102,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "EF8EF8B8-2BA6-473D-B22B-F959DE4AA4E1",
										"kind": "text",
										"name": "title2",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 239,
											"y": 1023,
											"width": 56,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Pipelines",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-ruy4ruy4.png",
											"frame": {
												"x": 239,
												"y": 1023,
												"width": 56,
												"height": 15
											}
										},
										"children": []
									},
									{
										"objectId": "7EFBF97B-505F-42A0-A04F-43B8503E046E",
										"kind": "group",
										"name": "underline2",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 228,
											"y": 1053,
											"width": 102,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-n0vgqky5.png",
											"frame": {
												"x": 228,
												"y": 1053,
												"width": 102,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "BFFD839A-BEBC-40FE-A07D-3B72E3C9F89E",
										"kind": "group",
										"name": "count_badge2",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 299,
											"y": 1021,
											"width": 21,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-qkzgrdgz.png",
											"frame": {
												"x": 299,
												"y": 1021,
												"width": 21,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "A96BDD0D-9E19-406D-96F9-6998F0CE442F",
										"kind": "group",
										"name": "bg2",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 228,
											"y": 1005,
											"width": 102,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-qtk2qkre.png",
											"frame": {
												"x": 228,
												"y": 1005,
												"width": 102,
												"height": 50
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "4E1AD5DE-4754-4B98-AD18-769D5CABCBC4",
								"kind": "group",
								"name": "changes",
								"originalName": "changes",
								"maskFrame": null,
								"layerFrame": {
									"x": 330,
									"y": 1005,
									"width": 100,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "234CDA89-D80D-4CAB-AEA3-B8E4F67009FC",
										"kind": "text",
										"name": "title3",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 340,
											"y": 1023,
											"width": 53,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Changes",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-mjm0q0rb.png",
											"frame": {
												"x": 340,
												"y": 1023,
												"width": 53,
												"height": 15
											}
										},
										"children": []
									},
									{
										"objectId": "66F00E87-6DCE-4AF6-8BD1-B5945A140ECB",
										"kind": "group",
										"name": "underline3",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 330,
											"y": 1053,
											"width": 100,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-njzgmdbf.png",
											"frame": {
												"x": 330,
												"y": 1053,
												"width": 100,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "810965D0-8EE7-44BC-B373-9826B6966260",
										"kind": "group",
										"name": "count_badge3",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 397,
											"y": 1021,
											"width": 21,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-odewoty1.png",
											"frame": {
												"x": 397,
												"y": 1021,
												"width": 21,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "D29A52F5-8E0D-47A1-BBEE-4A630BF4820E",
										"kind": "group",
										"name": "bg3",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 330,
											"y": 1005,
											"width": 100,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-rdi5qtuy.png",
											"frame": {
												"x": 330,
												"y": 1005,
												"width": 100,
												"height": 50
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "61BA60BC-114A-4853-923E-9ED859A68893",
						"kind": "group",
						"name": "tab_content",
						"originalName": "tab_content",
						"maskFrame": null,
						"layerFrame": {
							"x": 12,
							"y": 1056,
							"width": 979,
							"height": 1076
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-tab_content-njfcqtyw.png",
							"frame": {
								"x": 12,
								"y": 1056,
								"width": 979,
								"height": 1076
							}
						},
						"children": [
							{
								"objectId": "6ED9EEA7-9C91-4427-B813-8E7717B9DA43",
								"kind": "group",
								"name": "discussion_tab",
								"originalName": "discussion_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 12,
									"y": 1057,
									"width": 979,
									"height": 1075
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-discussion_tab-nkveouvf.jpg",
									"frame": {
										"x": 12,
										"y": 1057,
										"width": 979,
										"height": 1075
									}
								},
								"children": []
							},
							{
								"objectId": "04CB1380-810B-4E08-85E1-AD3AEDC1EB6C",
								"kind": "group",
								"name": "commits_tab",
								"originalName": "commits_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 10,
									"y": 1055,
									"width": 970,
									"height": 91
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-commits_tab-mdrdqjez.jpg",
									"frame": {
										"x": 10,
										"y": 1055,
										"width": 970,
										"height": 91
									}
								},
								"children": []
							},
							{
								"objectId": "A441A863-8CE5-4536-8C30-4A0654F358C2",
								"kind": "group",
								"name": "pipelines_tab",
								"originalName": "pipelines_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 5,
									"y": 1057,
									"width": 977,
									"height": 170
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-pipelines_tab-qtq0mue4.jpg",
									"frame": {
										"x": 5,
										"y": 1057,
										"width": 977,
										"height": 170
									}
								},
								"children": []
							},
							{
								"objectId": "AC49C334-D3A1-4BE9-AC6F-A2C195760F9D",
								"kind": "group",
								"name": "changes_tab",
								"originalName": "changes_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": -16,
									"y": 1054,
									"width": 1027,
									"height": 9120
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-changes_tab-qum0oumz.jpg",
									"frame": {
										"x": -16,
										"y": 1054,
										"width": 1027,
										"height": 9120
									}
								},
								"children": []
							},
							{
								"objectId": "8B09DABC-E596-459C-B09E-415348021EB7",
								"kind": "group",
								"name": "mergestatus_tab",
								"originalName": "mergestatus_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 15,
									"y": 1056,
									"width": 960,
									"height": 329
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-mergestatus_tab-oeiwourb.png",
									"frame": {
										"x": 15,
										"y": 1056,
										"width": 960,
										"height": 329
									}
								},
								"children": [
									{
										"objectId": "08291113-E800-4123-888D-55FBD778A958",
										"kind": "group",
										"name": "stop_environment_button_copy_22",
										"originalName": "stop-environment-button copy 2",
										"maskFrame": null,
										"layerFrame": {
											"x": 574,
											"y": 1185,
											"width": 99,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-stop_environment_button_copy_2-mdgyotex.png",
											"frame": {
												"x": 574,
												"y": 1185,
												"width": 99,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "058DCC7E-CF78-43E4-A9C6-BB67D540614B",
										"kind": "group",
										"name": "Group_231",
										"originalName": "Group 23",
										"maskFrame": null,
										"layerFrame": {
											"x": 509,
											"y": 1185,
											"width": 55,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 0.35
										},
										"image": {
											"path": "images/Layer-Group_23-mdu4rend.png",
											"frame": {
												"x": 509,
												"y": 1185,
												"width": 55,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "F22DE87E-AE79-44D7-902D-C085FC87C1A1",
										"kind": "text",
										"name": "command_line_instructions1",
										"originalName": "command-line-instructions",
										"maskFrame": null,
										"layerFrame": {
											"x": 68,
											"y": 1298,
											"width": 466,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "You can also merge or inspect manually using the command line instructions",
											"css": [
												"/* You can also merge o: */",
												"font-family: SourceSansPro-It;",
												"font-size: 15px;",
												"color: #333333;",
												"letter-spacing: 0;",
												"line-height: 20px;"
											]
										},
										"image": {
											"path": "images/Layer-command_line_instructions-rjiyreu4.png",
											"frame": {
												"x": 68,
												"y": 1298,
												"width": 466,
												"height": 15
											}
										},
										"children": []
									},
									{
										"objectId": "65603DB6-A7E0-43F0-8CF8-FCF9DA34C27C",
										"kind": "group",
										"name": "Group_9_Copy_51",
										"originalName": "Group 9 Copy 5",
										"maskFrame": null,
										"layerFrame": {
											"x": 31,
											"y": 1073,
											"width": 22,
											"height": 22
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9_Copy_5-nju2mdne.png",
											"frame": {
												"x": 31,
												"y": 1073,
												"width": 22,
												"height": 22
											}
										},
										"children": []
									},
									{
										"objectId": "8A29CF89-0BAD-472B-8853-51C28BAB815B",
										"kind": "group",
										"name": "Group_91",
										"originalName": "Group 9",
										"maskFrame": null,
										"layerFrame": {
											"x": 31,
											"y": 1186,
											"width": 22,
											"height": 22
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9-oeeyoung.png",
											"frame": {
												"x": 31,
												"y": 1186,
												"width": 22,
												"height": 22
											}
										},
										"children": []
									},
									{
										"objectId": "5F1EB55A-3512-4DA8-B6E8-53B8642D39F8",
										"kind": "group",
										"name": "Group_181",
										"originalName": "Group 18",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 1071,
											"width": 958,
											"height": 213
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_18-nuyxrui1.png",
											"frame": {
												"x": 16,
												"y": 1071,
												"width": 958,
												"height": 213
											}
										},
										"children": [
											{
												"objectId": "14D002F4-C500-479F-8B76-4194FD42994D",
												"kind": "group",
												"name": "Group_212",
												"originalName": "Group 21",
												"maskFrame": null,
												"layerFrame": {
													"x": 31,
													"y": 1071,
													"width": 387,
													"height": 27
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_21-mtremday.png",
													"frame": {
														"x": 31,
														"y": 1071,
														"width": 387,
														"height": 27
													}
												},
												"children": [
													{
														"objectId": "D16F608E-0FBB-4281-9D85-34F535E9EF93",
														"kind": "group",
														"name": "Group_9_Copy_31",
														"originalName": "Group 9 Copy 3",
														"maskFrame": null,
														"layerFrame": {
															"x": 31,
															"y": 1073,
															"width": 22,
															"height": 22
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy_3-rde2rjyw.png",
															"frame": {
																"x": 31,
																"y": 1073,
																"width": 22,
																"height": 22
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "895E1984-F3D2-4715-B4E9-1AAD2EF6D086",
												"kind": "group",
												"name": "Group_201",
												"originalName": "Group 20",
												"maskFrame": null,
												"layerFrame": {
													"x": 17,
													"y": 1103,
													"width": 956,
													"height": 38
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_20-odk1rte5.png",
													"frame": {
														"x": 17,
														"y": 1103,
														"width": 956,
														"height": 38
													}
												},
												"children": [
													{
														"objectId": "6250C6EB-071D-484A-89C0-1A58EE936512",
														"kind": "group",
														"name": "Group_221",
														"originalName": "Group 22",
														"maskFrame": null,
														"layerFrame": {
															"x": 58,
															"y": 1112,
															"width": 62,
															"height": 22
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_22-nji1mem2.png",
															"frame": {
																"x": 58,
																"y": 1112,
																"width": 62,
																"height": 22
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "7D9CC669-0644-42AB-8F6C-06DA1B0E85CC",
												"kind": "group",
												"name": "MR_body_text1",
												"originalName": "MR-body-text",
												"maskFrame": null,
												"layerFrame": {
													"x": 67,
													"y": 1135,
													"width": 206,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-MR_body_text-n0q5q0m2.png",
													"frame": {
														"x": 67,
														"y": 1135,
														"width": 206,
														"height": 15
													}
												},
												"children": [
													{
														"objectId": "1F5B3437-08F3-4C73-95D4-FB78847E0902",
														"kind": "group",
														"name": "stop_environment_button_copy1",
														"originalName": "stop-environment-button copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 448,
															"y": 1124,
															"width": 130,
															"height": 24
														},
														"visible": false,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-stop_environment_button_copy-muy1qjm0.png",
															"frame": {
																"x": 448,
																"y": 1124,
																"width": 130,
																"height": 24
															}
														},
														"children": []
													},
													{
														"objectId": "1A091ED7-704B-46CE-9AF3-DC420FED0265",
														"kind": "group",
														"name": "environment_buttons1",
														"originalName": "environment buttons",
														"maskFrame": null,
														"layerFrame": {
															"x": 62,
															"y": 1156,
															"width": 475,
															"height": 127
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"children": [
															{
																"objectId": "B6AEE0C6-7E3C-46A0-9BBB-23EAD7332881",
																"kind": "group",
																"name": "Group_141",
																"originalName": "Group 14",
																"maskFrame": null,
																"layerFrame": {
																	"x": 481,
																	"y": 1156,
																	"width": 56,
																	"height": 26
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "20F3C064-A81E-4E46-8BEF-C166A1D9B93A",
																		"kind": "group",
																		"name": "stop_environment_button_copy_31",
																		"originalName": "stop-environment-button copy 3",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 511,
																			"y": 1157,
																			"width": 26,
																			"height": 25
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_3-mjbgm0mw.png",
																			"frame": {
																				"x": 511,
																				"y": 1157,
																				"width": 26,
																				"height": 25
																			}
																		},
																		"children": []
																	},
																	{
																		"objectId": "CCCB8208-8DF4-4415-AA63-0F94AC269CAB",
																		"kind": "group",
																		"name": "stop_environment_button_copy_41",
																		"originalName": "stop-environment-button copy 4",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 481,
																			"y": 1157,
																			"width": 31,
																			"height": 25
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_4-q0ndqjgy.png",
																			"frame": {
																				"x": 481,
																				"y": 1157,
																				"width": 31,
																				"height": 25
																			}
																		},
																		"children": []
																	}
																]
															},
															{
																"objectId": "F4764988-AF2E-4410-B2F9-FBC2759BC77B",
																"kind": "group",
																"name": "Group_13_Copy1",
																"originalName": "Group 13 Copy",
																"maskFrame": null,
																"layerFrame": {
																	"x": 62,
																	"y": 1258,
																	"width": 399,
																	"height": 25
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "F04725D5-4E88-4896-A5AB-0A4CC630C29D",
																		"kind": "group",
																		"name": "stop_environment_button_copy_51",
																		"originalName": "stop-environment-button copy 5",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 62,
																			"y": 1258,
																			"width": 159,
																			"height": 25
																		},
																		"visible": false,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_5-rja0nzi1.png",
																			"frame": {
																				"x": 62,
																				"y": 1258,
																				"width": 159,
																				"height": 25
																			}
																		},
																		"children": []
																	},
																	{
																		"objectId": "58FCB48C-1A7C-4D55-9B30-F64678D47DA0",
																		"kind": "group",
																		"name": "stop_environment_button_copy_23",
																		"originalName": "stop-environment-button copy 2",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 302,
																			"y": 1258,
																			"width": 159,
																			"height": 25
																		},
																		"visible": false,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_2-nthgq0i0.png",
																			"frame": {
																				"x": 302,
																				"y": 1258,
																				"width": 159,
																				"height": 25
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													},
													{
														"objectId": "32C0BFED-7976-4DBF-8384-9E45699B1DAA",
														"kind": "group",
														"name": "Group_131",
														"originalName": "Group 13",
														"maskFrame": null,
														"layerFrame": {
															"x": 721,
															"y": 1070,
															"width": 243,
															"height": 30
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"children": [
															{
																"objectId": "2EDDCB9C-EBD0-4488-811A-08947568AC42",
																"kind": "group",
																"name": "Group_61",
																"originalName": "Group 6",
																"maskFrame": null,
																"layerFrame": {
																	"x": 721,
																	"y": 1070,
																	"width": 243,
																	"height": 30
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_6-mkverenc.png",
																	"frame": {
																		"x": 721,
																		"y": 1070,
																		"width": 243,
																		"height": 30
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "D098588D-7930-422B-A481-2FB372B6B567",
														"kind": "group",
														"name": "Group_41",
														"originalName": "Group 4",
														"maskFrame": null,
														"layerFrame": {
															"x": 477,
															"y": 1155,
															"width": 30,
															"height": 30
														},
														"visible": false,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_4-rda5odu4.png",
															"frame": {
																"x": 477,
																"y": 1155,
																"width": 30,
																"height": 30
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "FBC5DDAE-96B3-4B34-B817-EE45226AC343",
												"kind": "group",
												"name": "Group_9_Copy_21",
												"originalName": "Group 9 Copy 2",
												"maskFrame": null,
												"layerFrame": {
													"x": 37,
													"y": 1194,
													"width": 10,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9_Copy_2-rkjdnure.png",
													"frame": {
														"x": 37,
														"y": 1194,
														"width": 10,
														"height": 11
													}
												},
												"children": []
											},
											{
												"objectId": "72735271-A12C-4648-BADA-ADB71C9AE4B8",
												"kind": "group",
												"name": "Group_9_Copy1",
												"originalName": "Group 9 Copy",
												"maskFrame": null,
												"layerFrame": {
													"x": 31,
													"y": 1129,
													"width": 22,
													"height": 22
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9_Copy-nzi3mzuy.png",
													"frame": {
														"x": 31,
														"y": 1129,
														"width": 22,
														"height": 22
													}
												},
												"children": []
											}
										]
									}
								]
							}
						]
					},
					{
						"objectId": "F8CEF4B6-61A3-43CD-9862-75FED7B8C23F",
						"kind": "group",
						"name": "award_emoji",
						"originalName": "award_emoji",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 943,
							"width": 958,
							"height": 62
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-award_emoji-rjhdruy0.png",
							"frame": {
								"x": 16,
								"y": 943,
								"width": 958,
								"height": 62
							}
						},
						"children": [
							{
								"objectId": "8AB4830C-DE78-404B-9A62-0EF7EE947D39",
								"kind": "group",
								"name": "thumbs_down_button",
								"originalName": "thumbs-down-button",
								"maskFrame": null,
								"layerFrame": {
									"x": 72,
									"y": 957,
									"width": 48,
									"height": 33
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-thumbs_down_button-oefcndgz.png",
									"frame": {
										"x": 72,
										"y": 957,
										"width": 48,
										"height": 33
									}
								},
								"children": [
									{
										"objectId": "0F8F29C2-CB38-4EDE-91CE-A2C89ADEBCE6",
										"kind": "group",
										"name": "$1f44e",
										"originalName": "1f44e",
										"maskFrame": null,
										"layerFrame": {
											"x": 82,
											"y": 965,
											"width": 14,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "68AAC17D-77F9-4B0A-9632-7AB253BC6880",
												"kind": "group",
												"name": "Group2",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 82.5,
													"y": 965,
													"width": 13,
													"height": 19
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group-njhbqumx.png",
													"frame": {
														"x": 82.5,
														"y": 965,
														"width": 13,
														"height": 19
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "4504E87A-A9E9-4D61-8476-6EF638B57BFF",
								"kind": "group",
								"name": "thumbs_up_button",
								"originalName": "thumbs-up-button",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 957,
									"width": 47,
									"height": 33
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-thumbs_up_button-nduwneu4.png",
									"frame": {
										"x": 16,
										"y": 957,
										"width": 47,
										"height": 33
									}
								},
								"children": [
									{
										"objectId": "60756B9D-B7FA-4F65-B23B-C4899C3FC595",
										"kind": "group",
										"name": "$1f44d",
										"originalName": "1f44d",
										"maskFrame": null,
										"layerFrame": {
											"x": 26,
											"y": 965,
											"width": 13,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "64589099-C25B-44AA-A424-E42220C31036",
												"kind": "group",
												"name": "Group3",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 26.5,
													"y": 965,
													"width": 13,
													"height": 19
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group-njq1odkw.png",
													"frame": {
														"x": 26.5,
														"y": 965,
														"width": 13,
														"height": 19
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "2E6B28D7-F589-4AC6-B270-42330E350989",
								"kind": "group",
								"name": "add_emoji_button",
								"originalName": "add-emoji-button",
								"maskFrame": null,
								"layerFrame": {
									"x": 129,
									"y": 957,
									"width": 61,
									"height": 32
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-add_emoji_button-mku2qji4.png",
									"frame": {
										"x": 129,
										"y": 957,
										"width": 61,
										"height": 32
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "9C9E6A11-701C-4AED-A8A9-C99A66044CFE",
						"kind": "group",
						"name": "description",
						"originalName": "description",
						"maskFrame": null,
						"layerFrame": {
							"x": 12,
							"y": 63,
							"width": 972,
							"height": 523
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-description-oum5rtzb.jpg",
							"frame": {
								"x": 12,
								"y": 63,
								"width": 972,
								"height": 523
							}
						},
						"children": []
					}
				]
			}
		]
	}
]